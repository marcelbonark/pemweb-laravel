<?php

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('qrcode', function () {
    $image = \QrCode::format('png')
        ->merge('images/laravel.png', 0.2, true)
        ->size(500)->errorCorrection('H')
        ->generate('Hello');
    return response($image)->header('Content-type', 'image/png');
});

Route::get('api-example', function () {
    //Contoh API tanpa autentikasi
    $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', 'http://www.splashbase.co/api/v1/images/random');

    $json =  $response->getBody();
    $arrayOfResponse = json_decode($json, true);
    var_dump($arrayOfResponse);

    //Contoh API dengan autentikasi
    $client = new \GuzzleHttp\Client();
    $response = $client->request(
        'GET',
        'https://api.unsplash.com/',
        [
            'auth' =>
            ['Client ID', 'Secret Key'],
            'Accept-Version' => "v1"
        ]
    );
    ?>
    <img src="<?= $arrayOfResponse["url"] ?>">
<?php
});

Route::post("/test", "TestController@submission");
Route::get('/', "ProductsController@index");
Route::post('/postProduct', "ProductsController@posthandling");
Route::get('/postProduct', function () {
    return view('errorvalidation');
});

Route::post('/submitProduct', "ProductsController@captchavalidation");
Route::get('/submitProduct', function () {
    return view('errorvalidation');
});
Route::get('/newProduct', function () {
    return view('formproduct');
});


Route::get('/products', "ProductsController@index");

Route::get('/bonar', function () {
    return view('bonar');
});

Route::get('/rifky', function () {
    return view('rifky');
});

//bagian dimana login dicek
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
