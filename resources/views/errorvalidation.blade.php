@extends("layouts.app")

@section("content")
<div class="grid-x grid-margin-y">
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="medium-12 cell">
        <span class="badge alert"><i class="fi-x"></i></span>
        {{$error}}
    </div>
    @endforeach
    @else
    <script>
        window.location.href = '{{url("/products")}}'; //using a named route
    </script>
    @endif
    <div class="medium-12 cell">
        <a href="/newProduct" class="button">
            Insert New
        </a>
        <a href="/products" class="button">
            View Data
        </a>
    </div>
</div>
@endsection