@extends("layouts.app")

@section("content")
<div class="grid-x grid-padding-x align-center">
    <div class="cell small-8 medium-6">
        <div class="callout large alert">
            <h5>Delete Confirmation</h5>
            <p>Data with these details has been selected to be deleted:</p>
            <table>
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($product as $key=>$value)
                    <tr>
                        <td>{{$key}}</td>
                        <td>{{$value}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <p>Are you sure you want to delete?</p>
            <form method="POST" id="selectProduct" action="submitProduct">
                @csrf
                <input type="hidden" name="checkProduct" value="{{$product['id']}}">
                <div class="g-recaptcha" data-sitekey="6Lddf6oUAAAAANQmmxIYKzFafX1JmUFZX4dv7Sva" style="margin-bottom: 10px"> </div>
                <div class=".text-center">
                    <input type="submit" name="submission" value="Delete" class="button alert">
                    <a href="/products" class="button">
                        Cancel
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://www.google.com/recaptcha/api.js">
</script>
@endsection