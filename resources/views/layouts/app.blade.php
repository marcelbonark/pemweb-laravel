<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <title>Pemrograman Web - Laravel</title>
    @include('defaults.css-load')
</head>

<body>
    <div class="top-bar" style="margin-bottom:10px">
        <div class="top-bar-left">
            <ul class="menu" data-dropdown-menu>
                <li class="menu-text">
                    <a href="/">
                        Home &nbsp
                        <i class="fi-home"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="top-bar-right">
            <ul class="dropdown menu" data-dropdown-menu>
                @guest
                <li class="active">
                    <a href="{{ route('login') }}">
                        Login
                    </a>
                </li>
                @if (Route::has('register'))
                <li>
                    <a href="{{ route('register') }}">
                        Register
                    </a>
                </li>
                @endif
                @else
                <li>
                    <a href="#">{{ Auth::user()->name }}</a>
                    <ul class="menu vertical">
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
                @endguest
            </ul>
        </div>
    </div>
    <div class="grid-container">
        @yield('content')
    </div>
    @include('defaults.js-load')
    @yield('additional-js')
</body>

</html>