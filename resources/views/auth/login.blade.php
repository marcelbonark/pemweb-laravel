@extends('layouts.app')

@section('content')
<div class="grid-x grid-padding-x align-center">
    <div class="cell small-6 medium-6 large-6">
        <div class="callout small primary">
            <h3>{{ __('Login Form') }}</h3>
            <form method="POST" action="{{ route('login') }}" data-abide novalidate>
                @csrf
                <div data-abide-error class="alert callout" role=alert @if($errors->any()) style="display: block;" @else style="display: none;" @endif>
                    <p><i class="fi-alert"></i> There are some errors in your login details.</p>
                </div>
                <div class="input-group">
                    <span class="input-group-label" for="email">Email &nbsp<i class="fi-mail"></i></span>
                    <input id="email" type="email" class="input-group-field @error('email') is-invalid-input @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus aria-describedby="emailError" @error('email') aria-invalid="true" @enderror>
                    <br />
                </div>
                <span id="emailError" data-form-error-for="email" class="form-error @error('email') is-visible @enderror">
                    @error('email')
                    <strong>{{ $message }}</strong>
                    @else
                    <strong>This fields contains error(s) and it is required</strong>
                    @enderror
                </span>
                <div class="input-group">
                    <span class="input-group-label" for="password">Password &nbsp<i class="fi-lock"></i></span>
                    <input id="password" type="password" class="input-group-field @error('password') is-invalid-input @enderror" name="password" required autocomplete="current-password" aria-describedby="passwordError" @error('password') aria-invalid="true" @enderror>
                    <br />
                </div>
                <span id="passwordError" data-form-error-for="password" class="form-error @error('password') is_visible @enderror">
                    @error('password')
                    <strong>{{ $message }}</strong>
                    @else
                    <strong>This fields contains error(s) and it is required</strong>
                    @enderror
                </span>
                <div style="display:table">
                    <p class="align-middle" style="display:table-cell;vertical-align: middle;">Remember Me? &nbsp</p>
                    <div class="switch large" style="display:table-cell;vertical-align: middle;">
                        <input class="switch-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="switch-paddle" for="remember">
                            <span class="show-for-sr">Remember me? </span>
                            <span class="switch-active" aria-hidden="true">Yes</span>
                            <span class="switch-inactive" aria-hidden="true">No</span>
                        </label>
                    </div>
                </div>
                <p>
                    <button type="submit" class="button primary">
                        {{ __('Login') }}
                    </button>
                </p>
            </form>
        </div>
    </div>
</div>
@endsection