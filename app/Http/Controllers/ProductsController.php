<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Product;

class ProductsController extends Controller
{
    public function index($message = NULL)
    {
        $products = Product::all();
        return view(
            "viewproducts",
            compact('products', 'message')
        );
    }

    public function posthandling(Request $request)
    {
        $command = $request["submission"];
        $id = $request["checkProduct"];
        if ($command == "Update") {
            $product = Product::find($id);
            return view(
                "formproduct",
                compact('product', 'command')
            );
        } else if ($command == "Delete") {
            $id = $request["checkProduct"];
            $product = Product::find($id)->toArray();

            return view(
                "confirmdelete",
                compact('product', 'command')
            );
        }
    }

    public function captchavalidation(Request $request)
    {
        if (!isset($request["g-recaptcha-response"]) || empty($request["g-recaptcha-response"])) {
            $errors = new MessageBag;
            $errors->add("submission_error", "Unknown Captcha Verification!");
            return view('errorvalidation')->withErrors($errors);
        } else {
            $verifyURL = "https://www.google.com/recaptcha/api/siteverify";
            $verifyURL .= "?secret=6Lddf6oUAAAAAAb1rjmk-0oGfYXcFcaZP3qXpY0b";
            $verifyURL .= "&response=" . $request["g-recaptcha-response"];
            $verifyURL .= "&remoteip=" . $_SERVER["REMOTE_ADDR"];
            $verifyResponse = file_get_contents($verifyURL);
            $responseParse = (array) json_decode($verifyResponse);
            if ($responseParse["success"] == false) {
                $errors = new MessageBag;
                $errors->add("submission_error", "CAPTCHA Validation Error!!!");
                return view('errorvalidation')->withErrors($errors);
            }
        }
        return $this->submithandling($request);
    }

    public function submithandling(Request $request)
    {
        $command = $request["submission"];
        $id = $request["checkProduct"];
        $message = NULL;
        if ($command == "Update") {
            $product = $this->update($request);
            $message = "Product with ID=" . $product->id . " successfully updated";
        } else if ($command == "Delete") {
            $product = $this->delete($request);
            $message = "Product with ID=" . $product . " successfully deleted";
        } else if ($command == "Insert") {
            $product = $this->update($request, "Insert");
            $message = "Product with ID=" . $product->id . " successfully created";
        } else {
            $errors = new MessageBag;
            $errors->add("submission_error", "No handling for this submission type");
            return view('errorvalidation')->withErrors($errors);
        }
        return $this->index($message);
    }

    public function update(Request $request, $mode = "Update")
    {
        $product = NULL;
        $validatedData = $request->validate([
            'productName' => 'string|required',
            'productQpu' => 'string|required',
            'productStock' => 'numeric|required|min:0',
            'productPrice' => 'numeric|required|min:0',
        ]);
        if ($mode == "Update") {
            $id = $request["checkProduct"];
            $product = Product::find($id);
        } else {
            $product = new Product;
        }
        $product->product_name = $request["productName"];
        $product->quantity_per_unit = $request["productQpu"];
        $product->stock = $request["productStock"];
        $product->price = $request["productPrice"];
        $product->save();
        return $product;
    }

    public function delete(Request $request)
    {
        $validatedData = $request->validate([
            'checkProduct' => 'exists:barang,id'
        ]);
        $id = $request["checkProduct"];
        $product = Product::find($id);
        $product->delete();
        return $id;
    }
}
