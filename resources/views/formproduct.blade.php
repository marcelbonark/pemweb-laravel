@extends("layouts.app")

@section("content")

<form method="POST" id="selectProduct" action="submitProduct">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="medium-12 cell">
                @csrf
                @if(isset($command))
                <h1>{{$command}} Product</h1>
                @else
                <h1>Insert Product</h1>
                @endif
                @if(isset($product->id))
                <input type="hidden" name='checkProduct' value='{{$product->id}}' required>
                @endif
            </div>
            <div class="medium-12 cell">
                <label>Name
                    <input type="text" placeholder="Write your product name" name="productName" value='{{$product->product_name ?? ""}}' required>
                </label>
            </div>
            <div class="medium-12 cell">
                <label>Quantity Per Unit
                    <input type="text" name="productQpu" placeholder="Write your quantity measurement" value='{{$product->quantity_per_unit ?? ""}}' required>
                </label>
            </div>
            <div class="medium-12 cell">
                <label>
                    Stock
                    <input type="number" name="productStock" placeholder="Insert your stock value" value='{{$product->stock ?? ""}}' required>
                </label>
            </div>
            <div class="medium-12 cell">
                <label>
                    Price
                    <input type="number" name="productPrice" placeholder="Insert your price value" value='{{$product->price ?? ""}}' required>
                </label>
            </div>
            <div class="g-recaptcha" data-sitekey="6Lddf6oUAAAAANQmmxIYKzFafX1JmUFZX4dv7Sva" style="margin-bottom: 10px"> </div>
            <div class="medium-12 cell">
                @if(isset($command))
                <input class="button success" type="submit" name="submission" value='{{$command}}'>
                <a href="/newProduct" class="button">
                    Insert New
                </a>
                @else
                <input class="button success" type="submit" name="submission" value='Insert'>
                @endif
                <a href="/products" class="button">
                    View Data
                </a>
            </div>
        </div>
    </div>
</form>
<script src="https://www.google.com/recaptcha/api.js">
</script>
@endsection