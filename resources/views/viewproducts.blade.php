@extends("layouts.app")

@section("content")
<div class="grid-x grid-margin-y">
    @if(isset($message))
    <div class="medium-12 cell">
        <span class="badge success"><i class="fi-check"></i></span>
        {{$message}}
    </div>
    @endif
    <div class="medium-12 cell">
        <form method="POST" id="selectProduct" action="postProduct">
            @csrf
            @auth
            <a href="/newProduct" class="button success">
                New Data
            </a>
            <input type="submit" name="submission" value="Update" class="button" disabled>
            <input type="submit" name="submission" value="Delete" class="alert button" disabled>
            @endauth
            <input type="hidden" id="checkProduct" name="checkProduct" value="">
            <table id="productTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Product Name</th>
                        <th>Quantity Per Unit</th>
                        <th>Stock</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{$product->product_name}}</td>
                        <td>{{$product->quantity_per_unit}}</td>
                        <td>{{$product->stock}}</td>
                        <td>{{$product->price}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
</div>
@endsection

@section('additional-js')
<script>
    $(function() {
        var table = $('#productTable').DataTable();

        @auth
        $('#productTable tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var idData = table.cell('.selected', 0).data();
                $("#checkProduct").val(idData);
            }
            if ($('#productTable').find('tr.selected').length) {
                $("input[type='submit']").prop("disabled", false);
            } else {
                $("input[type='submit']").prop("disabled", true);
            }
        });
        @endauth
    });
</script>
@endsection